# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################



from odoo import models, fields, api, _

class SaleOrderInherited(models.Model):
    # uncomment following if this model is new model, and not based on any existing
    #_name = '<model.xyz>'
    
    # uncomment following if this model is based on existing model
    _inherit = 'sale.order'

    # following are templates for different field types, 
    # uncomment and edit name
    
    x_price_unit = fields.Float('Bruttohinta',)
    #x_coef = fields.Float('Coef', default=0.65, )
    x_net_price = fields.Float(compute='_compute_x_net_price')
    
    @api.depends('x_price_unit', )
    def _compute_x_net_price(self):
        for record in self:
            record.x_net_price = record.x_price_unit * 0.65
        
    
   # <integer_field> = fields.Integer('Field name', help='Help text')
   # <float_field> = fields.Float('Field name', help='Help text')
   # <date_field> = fields.Date('Field name', help='Help text')
   # <datetime_field> = fields.Datetime('Field name', help='Help text')
    
    # many2one field is a field to points another object, e.g. partner on order object
    #<many2one_field> = fields.Many2one('<father.model.name>', 'Pickup company', 'Field name', help='Help text')

    # many2one field is a field to points another object, e.g. partner on order object
    #<one2many_field> = fields.One2many('<child.model.name>', '<id_field>', 'Field name', help='Help text')

    # many2many is most complex object relation, and it requires separate link table
    # link table name is given in field declaration, must ne unique in db
   # <many2many_field> = fields.Many2many('<related.object.name>', '<this_object_related_object_rel>', 
       # '<this_object_id>', '<related_object_id>', string='Field name', help='Help text')
    